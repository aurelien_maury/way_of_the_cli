# Koan 01

Utiliser curl pour télécharger le fichier situé à l'adresse :

* https://bitbucket.org/aurelien_maury/way_of_the_cli/raw/539192261508fb90ee2d0480e7c91b0dbf375118/koan_01/DB.csv

Puis, successivement :

* Utiliser la commande cat pour afficher son contenu
* Utiliser des pipes, pour : 
  * découper chaque ligne en morceau, avec le caractère ";" comme séparateur et n'afficher que le 3e champ (commande cut)
  * filtrer les lignes contenant le caractère "=" (commande grep)
  * filtrer les lignes contenant le caractère "+" (commande grep)
  * remplacer tous les retours à la lignes par des espaces (commande tr)