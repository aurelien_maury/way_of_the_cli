# Pipe, pipe, pipe

```
echo ".#ibbar e#ihX eh# X-ll-F"
```

En partant de là, batir une chaine de commande pipée pour

* transformer tout les caractères "X" en "w"
* transformer tout les caractères "#" en "t"
* transformer tout les caractères "-" en "o"
* inverser la chaine de caractère

