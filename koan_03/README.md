# Repetition is the mother of learning

```
curl -s  https://baconipsum.com/api/?type=meat-and-filler
```

A partir de cette commande, compter le nombre de fois que le mot "bacon" apparait dans le résultat.